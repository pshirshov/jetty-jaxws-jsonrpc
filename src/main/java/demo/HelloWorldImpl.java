package demo;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(endpointInterface = "demo.HelloWorld"
        , serviceName = "demo.HelloWorld"
        , targetNamespace = "http://album.jsonplugin.com/json/"
)
//@BindingType(JSONBindingID.JSON_BINDING)
public class HelloWorldImpl implements HelloWorld {
    public
    @WebResult(name = "message")
    String sayHi(@WebParam(name = "name") String text) {
        System.out.println("sayHi called");
        return "Hello " + text + "@" + Thread.currentThread().getName();
    }
}
// END SNIPPET: service
