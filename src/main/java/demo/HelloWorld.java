package demo;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

// START SNIPPET: service

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL)
public interface HelloWorld {

    String sayHi(String text);
}
// END SNIPPET: service
