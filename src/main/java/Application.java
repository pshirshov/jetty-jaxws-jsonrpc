import com.jaxws.json.codec.JSONBindingID;
import com.sun.xml.ws.developer.BindingTypeFeature;
import demo.HelloWorld;
import demo.HelloWorldImpl;
import org.eclipse.jetty.http.spi.JettyHttpServerProvider;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.HandlerCollection;

import javax.xml.ws.Endpoint;

/**
 * Date: 02.07.12
 * Time: 14:46
 * <p/>
 * http://tech-eureka.blogspot.com/2009/10/jax-ws-inside-jetty.html
 * http://www.mkyong.com/webservices/jax-ws/wrapper-class-package-jaxws-methodname-is-not-found-have-you-run-apt-to-generate-them/
 */
public class Application {
    public static void main(String[] args) throws Exception {
        // 1.2) make sure JAX-WS endpoint.publish will use our new service provider: JettyHttpServerProvider
        System.setProperty("com.sun.net.httpserver.HttpServerProvider",
                "org.eclipse.jetty.http.spi.JettyHttpServerProvider");

        int port = 8080;
        Server jettyServer = new Server(port);

        /** 1) Publish WebService (JettyHttpServerProvider) */
        // 1.1) register THIS Jetty server with the JettyHttpServerProvider
        JettyHttpServerProvider.setServer(jettyServer);

        // 1.3) add an empty HandlerCollection to by setup by this provider
        HandlerCollection handlerCollection = new HandlerCollection();
        handlerCollection.addHandler(new ContextHandlerCollection());
        jettyServer.setHandler(handlerCollection);


        HelloWorld svc = new HelloWorldImpl();

        Endpoint.create(svc);
        String context = "/web/json";
        Endpoint.publish("http://localhost:" + port + context, svc,
                new BindingTypeFeature(JSONBindingID.JSON_BINDING));

        String contextSoap = "/web/soap";
        Endpoint.publish("http://localhost:" + port + contextSoap, svc);

        jettyServer.start();
        jettyServer.join();
    }
}
